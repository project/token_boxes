Major branch for version 6.x-1.x.

1. Usage
------------------------------------------------------------------------------
Typical use cases are to allow users to insert boxes (blocks) into content fields, using token_filter. Another example is to have configurable pieces of content in the header or footer area of a view.

2. Related modules
------------------------------------------------------------------------------
This module depends on:

Boxes - http://drupal.org/project/boxes
Token - http://drupal.org/project/token
This module works well with:

Token Filter - http://drupal.org/project/token_filter
Block Edit - http://drupal.org/project/block_edit

3. Examples of usage
------------------------------------------------------------------------------
If you want to allow users to insert a box's content into the body of your content, you can use token filter. First you'll want to enable token filter, and then apply token filter to all input filters you are exposing for that content field. Input formats are configurable at Administer -> Site configuration -> Input Formats -> List (admin/settings/filters). Click the "configure" link, and check the "Token filter" box under "Filters". Do this for every input format you want tokens to be evaluated.

Next, create your box, which is really just a block with some extra mojo (such as a machine name) that allow them to be exported (Chaos tool suite (ctools), Features).

The token can be found on the help page for Tokens, Help -> Token (help/token). Expand "Boxes". You should see all boxes created on your site. The token will look like:
[<box-machine-name>], where <box-machine-name> is the machine name you gave the box when you created it.

Here's the tricky part. To add this token to your content body, you'll need to add token boxes to the beginning of the token. So your token will look like this:
[token boxes <box-machine-name>]

For example, if you create a box with a machine name of "foo_bar", you'll see this in the help in the Boxes category:
[foo_bar]
and you'll need to put this into the body of your content:
[token boxes foo_bar]

This works the same in view header and footer fields, since they have input filters on them.

